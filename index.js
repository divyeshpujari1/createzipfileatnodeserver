'use strict';
const AdmZip = require('adm-zip');
const zip = new AdmZip();

// Adding a file in zip.
zip.addLocalFile('./1.txt');
zip.addLocalFile('./images.png');

// Write a zip file.
zip.writeZip('./target.zip');