### Create/Write Zip On Node Server Side.

- This repository include the `POC` of creating zip file at `Node` environment.

#### Technical Note:
- `addLocalFile` This method is use to add the file for zip.
- `writeZip` This method is use to write or create the zip file.
